using BackData.Domain.Models.Database;
using BackData.Jobs;
using BackData.Models;
using log4net;
using Quartz;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace BackData.Services;

public static class BackupService
{
    private static readonly ILog Logger;

    static BackupService()
    {
        Logger = LogManager.GetLogger(typeof(BackupService));
    }

    /// <summary>
    ///     Register all jobs for Quartz
    /// </summary>
    /// <param name="schedulerFactory"></param>
    /// <exception cref="ApplicationException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public static async Task RunAllBackUp(ISchedulerFactory schedulerFactory)
    {
        Logger.Info(Environment.NewLine);
        Logger.Info("########################");
        Logger.Info("Register all jobs ");
        var scheduler = await schedulerFactory.GetScheduler();

        var conf = GetConf();

        foreach (var database in conf.Databases)
        {
            Logger.Info($"Try to add job {database.Host}.{database.JobName} with {database.TypeDatabaseString} driver");
            var jobKey = JobKey.Create(database.JobName, $"{database.Host}");
            var jobBuilder = database.TypeDatabase switch
            {
                TypeDatabaseEnum.Mysql => JobBuilder.Create<MysqlJob>().WithIdentity(jobKey),
                TypeDatabaseEnum.PostgreSql => throw new ApplicationException("Not supported at this time"),
                TypeDatabaseEnum.SqlServer => throw new ApplicationException("Not supported at this time"),
                _ => throw new ArgumentOutOfRangeException()
            };

            var job = jobBuilder.StoreDurably().Build();
            job.JobDataMap.Put("database", database);

            await scheduler.AddJob(job, false);

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{database.DatabaseName}.{Guid.NewGuid()}", $"{database.Host}")
                .StartNow()
                .WithCronSchedule("0 * * * * ?")
                // .WithCronSchedule(database.CronSchedule)
                .ForJob(job)
                .Build();

            var triggerSartup = TriggerBuilder.Create()
                .WithIdentity($"STARTUP.{database.DatabaseName}.{Guid.NewGuid()}", $"{database.Host}")
                .StartNow()
                .WithSimpleSchedule(builder => builder.WithRepeatCount(0))
                .ForJob(job)
                .Build();

            await scheduler.ScheduleJob(trigger);
            await scheduler.ScheduleJob(triggerSartup);
        }
        
        if(conf.Databases.Count == 0) Logger.Error("No database detected.");

        Logger.Info("--- Start scheduler ---");
        await scheduler.Start();
    }

    private static BackDataConfModel GetConf()
    {
        var deserializer = new DeserializerBuilder()
            .WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build();
        var yamlConfig = File.ReadAllText(BackDataGlobal.DefaultLocationConf);

        return deserializer.Deserialize<BackDataConfModel>(yamlConfig);
    }
}