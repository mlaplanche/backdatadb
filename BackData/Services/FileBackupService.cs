using System.Text;
using BackData.Domain.Models.Database;

namespace BackData.Services;

public static class FileBackupService
{
    static FileBackupService()
    {
        if (!Directory.Exists(BackDataGlobal.DefaultDirectoryBackup))
            Directory.CreateDirectory(BackDataGlobal.DefaultDirectoryBackup);
    }

    public static void CreateBackupFile(DatabaseModel database, string backup)
    {
        var filename =
            $"{BackDataGlobal.DefaultDirectoryBackup}/{database.Host}-{database.DatabaseName}-{DateTimeOffset.Now.ToString("yyyy-MM-dd_HH-mm-ss")}.sql";

        using var fs = File.Create(filename);
        fs.Write(new UTF8Encoding(true).GetBytes(backup));

        ClearBackupfile($"{database.Host}-{database.DatabaseName}*.sql");
    }

    private static void ClearBackupfile(string pattern)
    {
        var directory = new DirectoryInfo(BackDataGlobal.DefaultDirectoryBackup);
        var allFiles = directory.GetFiles(pattern);

        var filesToDelete = allFiles
            .Where(f => f.CreationTimeUtc < DateTime.UtcNow.AddMinutes(-2));
            // .Where(f => f.CreationTimeUtc < DateTime.UtcNow.AddMonths(-2));

        foreach (var fileInfo in filesToDelete)
        {
            fileInfo.Delete();
        }
    }
}