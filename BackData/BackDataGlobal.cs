namespace BackData;

public static class BackDataGlobal
{
    public static string DefaultDirectoryConf { get; set; } = "./conf";
    public static string DefaultFilenameConf { get; set; } = "backdata.yml";

    public static string DefaultLocationConf => $"{DefaultDirectoryConf}/{DefaultFilenameConf}";

    public static string DefaultDirectoryBackup { get; set; } = "./backup";

}