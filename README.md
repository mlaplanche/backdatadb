# BackData

[![GitLab Activity](https://img.shields.io/gitlab/last-commit/mlaplanche/backdata?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge)](https://gitlab.com/mlaplanche/backdata/activity)
[![GitLab license](https://img.shields.io/gitlab/license/mlaplanche/backdata?style=for-the-badge)](https://gitlab.com/mlaplanche/backdata)
[![Gitlab build](https://img.shields.io/gitlab/pipeline-status/mlaplanche/backdata?branch=develop&style=for-the-badge)](https://gitlab.com/mlaplanche/backdata)
[![Docker pulls](https://img.shields.io/docker/pulls/maximelaplanche/backdata?logo=docker&style=for-the-badge)](https://hub.docker.com/r/maximelaplanche/backdata)
[![Docker size](https://img.shields.io/docker/image-size/maximelaplanche/backdata?logo=docker&sort=semver&style=for-the-badge)](https://hub.docker.com/r/maximelaplanche/backdata)
[![Docker version](https://img.shields.io/docker/v/maximelaplanche/backdata?logo=docker&sort=semver&style=for-the-badge)](https://hub.docker.com/r/maximelaplanche/backdata)
[![GitLab release](https://img.shields.io/gitlab/v/release/mlaplanche/backdata?sort=semver&style=for-the-badge)](https://img.shields.io/gitlab/v/release/mlaplanche/backdata?sort=semver&style=for-the-badge)

Docker container to Backup your Mysql & Postgres databases.

## About

BackData is a docker container allowing the backup of your databases in an automated way. The interval is fully customizable via the Cron syntax.

This project is realized in [.NET](https://dotnet.microsoft.com/en-us/download) and relies on the [Quartz](https://www.quartz-scheduler.net/) tool for the scheduled jobs.

**For the moment, this container only supports MySQL databases.**

## Maintainer

* [Maxime LAPLANCHE](https://github.com/LaplancheMaxime)

## Prerequisites and Assumptions

You must have a database accessible from the BackData container with the right credentials.

When BackData is launched, it makes a backup of all the configured databases.
This allows it to check the connections and start from a correct base.

## Usages

### Persitent storage

The following directories are used for the configuration and data of the container, they must be persistent.

| Directory              | Description         |
| ---------------------- | ------------------  |
| `/app/backup`          |  Backups            |
| `/app/conf`            | Configuration files |

### Environment Variables

**Warning: If you use environment variables, the functionality is limited. Go to the file-based configuration section for advanced features.**

#### Backup options

| Env. var          | Description                          | Default           |
| ---------         | -----------                          | --------          |
| `DB_TYPE`         | Db server type to backup : `mysql`   | mysql             |
| `DB_HOST`         | Name of db server to backup          |                   |
| `DB_PORT`         | Port of db server to backup          | 3306              |
| `DB_NAME`         | Name of db to backup                 |                   |
| `DB_USER`         | User of db to backup                 |                   |
| `DB_PASSWORD`     | Password of db to backup             |                   |
| `DB_BACKUP_CRON`  | Cron expression to run backup        | `0 0 12,0 * * ?`  |

If you want more information about the cron syntax used, please visit this [website](http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/crontrigger.html).

Docker-compose exemple:

````yml
---
version: '3.7'

services:
  db:
    image: mysql
    # NOTE: use of "mysql_native_password" is not recommended: https://dev.mysql.com/doc/refman/8.0/en/upgrading-from-previous-series.html#upgrade-caching-sha2-password
    # (this is just an example, not intended to be a production configuration)
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: rootp@ss0rd
      MYSQL_DATABASE: myDatabase
      MYSQL_USER: test_usr
      MYSQL_PASSWORD: p@ssw0rd

  backdata:
    image: maximelaplanche/backdata
    restart: always
    environement:
      DB_TYPE: mysql
      DB_HOST: db
      DB_PORT: 3306
      DB_NAME: myDatabase
      DB_USER: test_usr
      DB_PASSWORD: p@ssw0rd

````

### Manual configuration

From a yaml file, you can configure BackData.
This file can be found here: `/app/conf/backdata.yml`

Look at this [file](./BackData/backdata.example.yml) for an example.

This file is loaded each time BackData is started.

## Roadmap

* Support for other database servers: `Postgresql`, `SQLServer`, etc..
* Send backups securely to third-party services: `ssh`, `sftp`, `ftp`, `OneDrive`, `S3 service compatible`



## License

See [LICENSE here](./LICENSE).

## Project status

Project still in development.
