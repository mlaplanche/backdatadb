namespace BackData.Domain.Models.Database;

public enum TypeDatabaseEnum
{
    Mysql,
    PostgreSql,
    SqlServer
}